#!/bin/bash

LOGIN=$1
GROUP=$2
NEWUID=$3
NEWGID=$4

OLDUID=`id -u $LOGIN`
OLDGID=`id -g $GROUP`

echo $NEWUID
echo $LOGIN

usermod -o -u $NEWUID $LOGIN
groupmod -o -g $NEWGID $GROUP
#find /var/ -user $OLDUID -exec chown -h $NEWUID {} \;
#find /var/ -group $OLDGID -exec chgrp -h $NEWGID {} \;
#find /home/ -user $OLDUID -exec chown -h $NEWUID {} \;
#find /home/ -group $OLDGID -exec chgrp -h $NEWGID {} \;
#find /usr/share/ -user $OLDUID -exec chown -h $NEWUID {} \;
#find /usr/share/ -group $OLDGID -exec chgrp -h $NEWGID {} \;
#find /etc/ -user $OLDUID -exec chown -h $NEWUID {} \;
#find /etc/ -group $OLDGID -exec chgrp -h $NEWGID {} \;
#find /opt/ -user $OLDUID -exec chown -h $NEWUID {} \;
#find /opt/ -group $OLDGID -exec chgrp -h $NEWGID {} \;
#find /run/ -group $OLDGID -exec chgrp -h $NEWGID {} \;
#find /run/ -user $OLDUID -exec chown -h $NEWUID {} \;
