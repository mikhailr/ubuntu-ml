#!/bin/bash
if [ -z "$CUID" ]; then
    echo "No uid changed"
    exit 0
fi 
 
echo $CUID

if [ -z "$CGID" ]; then
    echo "No guid changed"
    exit 0
fi  

echo $CGID
while read NAME
do
    echo "$NAME"
    /bin/change_permitions.sh $NAME $NAME $CUID $CGID
done < /etc/fix_perm_users

mv /etc/fix_perm_users /etc/fix_perm_users.done
touch /etc/fix_perm_users
